# 4G_Backhaul_RPI
##Quectel 4g module with raspberry pi (USB)



#1.Update and Install the QMI Interface
   

sudo apt-get update
sudo apt-get install raspberrypi-kernel-headers

#2.get qmi installer 

wget https://gitlab.com/sudeeshvs01/4g_backhaul_rpi/-/raw/master/installer/qmi_install.sh

sudo chmod +x qmi_install.sh

sudo ./qmi_install.sh

#3.enable auto connect

wget https://gitlab.com/sudeeshvs01/4g_backhaul_rpi/-/raw/master/installer/install_auto_connect.sh

sudo chmod +x install_auto_connect.sh

sudo ./install_auto_connect.sh